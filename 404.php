<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php bloginfo('title'); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="You">
        <?php
            //Responseive grid + ie fix
            wp_enqueue_style( 'rgt-ie',
                               get_bloginfo('template_url') . '/css/responsive-grid.css');
            wp_enqueue_style( 'rgt-ie',
                               get_bloginfo('template_url') . '/css/ie.css');
            //Main stylesheet
            wp_enqueue_style( 'main-theme-style',
                               get_bloginfo('template_url') . '/style.css');
            //Include the mobile nav script
            wp_enqueue_script( 'mobile-nav',
                               $src = get_bloginfo('template_url') . '/js/mobile-nav.js',
                               $deps = array('jquery'),
                               $ver = false,
                               $in_footer = true );
           ?>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="<?php bloginfo('template_url'); ?>/js/html5shiv.js"></script>
        <![endif]-->
        <?php wp_head(); ?>
    </head>
    <body>
        <div id="page_wrap" class="container row">
            <header class="row">
                  <?php get_header(); ?>
              </header>


              <section id="main-content" class="row">
                  <article id="page-content" class="col span_8">
                    <h1>Oh dear...</h1>
                    <h2>This is not the page you were looking for... Sorry! :c</h2>

                </article>
                <section id="sidebar"class="col span_4">
                      <?php get_sidebar(); ?>
                  </section>
            </section>

            <footer class="row">
                  <?php get_footer(); ?>
              </footer>
        </div>
        <?php wp_footer(); ?>
        <?php get_template_part( 'analytics', 'google' ); ?>
      </body>
</html>