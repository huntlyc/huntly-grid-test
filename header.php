<nav>
    <a id="logo" href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/img/logo.png" alt="logo"/></a>
    <a id="mobile-menu">Menu</a>
    <ul>
        <?php wp_list_pages( array( 'depth' => 1,'sort_column' => 'menu_order','title_li' => '' ) ); ?>
    </ul>
</nav>